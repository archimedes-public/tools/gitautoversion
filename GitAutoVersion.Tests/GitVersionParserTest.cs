using NUnit.Framework;
using GitAutoVersion;
using System.Collections.Generic;

namespace GitAutoVersion.Tests
{
    public class GitVersionParserTest
    {
        public List<GitCommit> Commits { get; set; }
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void CanGetEmpty()
        {
            Commits = new List<GitCommit>();
            var log = Commits.GetVersionLog();
            Assert.AreEqual(0, log.Count, "Log should be empty");
            Assert.AreEqual(0, log.GetVersion().Patch, "Patch should be 0");
        }
        [Test]
        public void CanIncreasePatch()
        {
            Commits = new List<GitCommit>() {
                new GitCommit() { Message = "fix:Message 1" }
            };
            var log = Commits.GetVersionLog();
            Assert.AreEqual(1, log.Count, "Log should have 1 entry");
            Assert.AreEqual(1, log.GetVersion().Patch, "Patch should be 1");
        }
        [Test]
        public void CanIncreaseAll()
        {
            Commits = new List<GitCommit>() {
                new GitCommit() { Message = "breaking change:Message 1" },
                new GitCommit() { Message = "breaking change:Message 2" },
                new GitCommit() { Message = "feat:Message 3" },
                new GitCommit() { Message = "feat:Message 4" },
                new GitCommit() { Message = "fix:Message 5" },
                new GitCommit() { Message = "fix:Message 6" }
            };
            var log = Commits.GetVersionLog();
            Assert.AreEqual(6, log.Count, "Log should have 6 entries");
            Assert.AreEqual(2, log.GetVersion().Patch, "Patch should be 1");
            Assert.AreEqual(2, log.GetVersion().Minor, "Minor should be 1");
            Assert.AreEqual(2, log.GetVersion().Major, "Major should be 1");
        }
        [Test]
        public void CanSortOutUnrelated()
        {
            Commits = new List<GitCommit>() {
                new GitCommit() { Message = "fix:Message 1" },
                new GitCommit() { Message = "NO FITTING MESSAGE" },
                new GitCommit() { Message = "fix:Message 2" }
            };
            var log = Commits.GetVersionLog();
            Assert.AreEqual(2, log.Count, "Log should have 2 enties");
            Assert.AreEqual(2, log.GetVersion().Patch, "Patch should be 2");
        }
        [Test]
        public void CanResetMinorAndPatch()
        {
            Commits = new List<GitCommit>() {
                new GitCommit() { Message = "fix:Message 1" },
                new GitCommit() { Message = "fix:Message 2" },
                new GitCommit() { Message = "fix:Message 3" }
            };
            var log = Commits.GetVersionLog();
            Assert.AreEqual(3, log.GetVersion().Patch, "Patch should be 3");
            Commits.Add(new GitCommit() { Message = "feat:Message 4" });
            Commits.Add(new GitCommit() { Message = "fix:Message 5" });
            log = Commits.GetVersionLog();
            Assert.AreEqual(1, log.GetVersion().Patch, "Patch should be 1");
            Assert.AreEqual(1, log.GetVersion().Minor, "Minor should be 1");
            Commits.Add(new GitCommit() { Message = "breaking change:Message 6" });
            log = Commits.GetVersionLog();
            Assert.AreEqual(0, log.GetVersion().Patch, "Patch should reset to 0");
            Assert.AreEqual(0, log.GetVersion().Minor, "Minor should reset to 0");
            Assert.AreEqual(1, log.GetVersion().Major, "Major should be 1");
        }
        [Test]
        public void CanSet()
        {
            Commits = new List<GitCommit>() {
                new GitCommit() { Message = "patch:10001" }
            };
            var log = Commits.GetVersionLog();
            Assert.AreEqual(10001, log.GetVersion().Patch, "Patch should be 10001");
            Commits.Add(new GitCommit() { Message = "minor:10001" });
            log = Commits.GetVersionLog();
            Assert.AreEqual(0, log.GetVersion().Patch, "Patch should be 0");
            Assert.AreEqual(10001, log.GetVersion().Minor, "Minor should be 10001");
            Commits.Add(new GitCommit() { Message = "major:10001" });
            log = Commits.GetVersionLog();
            Assert.AreEqual(10001, log.GetVersion().Major, "Major should be 10001");
            Assert.AreEqual(0, log.GetVersion().Minor, "Minor should be 0");
            Assert.AreEqual(0, log.GetVersion().Patch, "Patch should be 0");
        }
        [Test]
        public void CanSetVersion()
        {
            Commits = new List<GitCommit>() {
                new GitCommit() { Message = "version:1" }
            };
            var log = Commits.GetVersionLog();
            Assert.AreEqual("1.0.0", log.GetVersion().ToString(), "Version Should be 1.0.0");
            Commits.Add(new GitCommit() { Message = "version:2.2" });
            log = Commits.GetVersionLog();
            Assert.AreEqual("2.2.0", log.GetVersion().ToString(), "Version Should be 2.2.0");
            Commits.Add(new GitCommit() { Message = "version:1.1.1" });
            log = Commits.GetVersionLog();
            Assert.AreEqual("1.1.1", log.GetVersion().ToString(), "Version Should be 1.1.1");
        }
        [Test]
        public void CanSetComparer()
        {
            Commits = new List<GitCommit>() {
                new GitCommit() { Message = "fix:Message 1" },
                new GitCommit() { Message = "NO FITTING MESSAGE" },
                new GitCommit() { Message = "fIx:Message 2" }
            };
            var log = Commits.GetVersionLog();
            Assert.AreEqual(2, log.Count, "Log length should be 2");
            Assert.AreEqual(2, log.GetVersion().Patch, "Patch should be 2");
            log = Commits.GetVersionLog(new GitVersionParserConfig() { StringComparison = System.StringComparison.Ordinal });
            Assert.AreEqual(1, log.Count, "Log length should be 1");
            Assert.AreEqual(1, log.GetVersion().Patch, "Patch should be 1");
        }
        [Test]
        public void CanSetMultipleNames()
        {
            Commits = new List<GitCommit>() {
                new GitCommit() { Message = "fix:Message 1" },
                new GitCommit() { Message = "quickfix:Message 2" },
                new GitCommit() { Message = "fix:Message 3" }
            };
            var log = Commits.GetVersionLog();
            Assert.AreEqual(2, log.Count, "Log length should be 2");
            Assert.AreEqual(2, log.GetVersion().Patch, "Patch should be 2");
            log = Commits.GetVersionLog(new GitVersionParserConfig() { PatchIncStrings = new string[] { "fix", "quickfix" } });
            Assert.AreEqual(3, log.Count, "Log length should be 3");
            Assert.AreEqual(3, log.GetVersion().Patch, "Patch should be 3");
        }
        [Test]
        public void CanSetSeperator()
        {
            Commits = new List<GitCommit>() {
                new GitCommit() { Message = "fix:Message 1" },
                new GitCommit() { Message = "fix=Message 2" },
                new GitCommit() { Message = "fix:Message 3" }
            };
            var log = Commits.GetVersionLog();
            Assert.AreEqual(2, log.Count, "Log length should be 2");
            Assert.AreEqual(2, log.GetVersion().Patch, "Patch should be 2");
            log = Commits.GetVersionLog(new GitVersionParserConfig() { Seperator = "=" });
            Assert.AreEqual(1, log.Count, "Log length should be 1");
            Assert.AreEqual(1, log.GetVersion().Patch, "Patch should be 1");
        }
    }
}