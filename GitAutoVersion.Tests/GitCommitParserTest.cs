﻿using NUnit.Framework;
using GitAutoVersion;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace GitAutoVersion.Tests
{
    public class GitCommitParserTest
    {
        public List<GitCommit> Commits { get; set; }
        [SetUp]
        public void Setup()
        {
            Commits = GitCommitParser.ReadOutput(System.IO.File.ReadAllText("gitLog.txt"));
        }

        [Test]
        public void CouldReadLogFile()
        {
            Assert.NotNull(Commits, "Should not be null");
            Assert.NotZero(Commits.Count, "Should not be empty");
        }
        [Test]
        public void CouldReadCommit()
        {
            Assert.AreEqual("Initial commit", Commits.FirstOrDefault().Message, "Should be 'Initial commit'");
            Assert.AreEqual("fix: Newest Fix", Commits.LastOrDefault().Message, "Should be 'fix: Newest Fix'");
        }
        [Test]
        public void CouldReadVersion()
        {
            Assert.AreEqual("0.6.1", Commits.GetVersionLog().GetVersion().ToString(), "Should be");
        }
        [Test]
        public void CouldReadRealLog()
        {
            Commits = GitCommitParser.GetGitLog();
            Assert.NotNull(Commits, "Should not be null");
            Assert.NotZero(Commits.Count, "Should not be empty");
        }
        [Test]
        public void CouldReadRealSpecific()
        {
            Commits = GitCommitParser.GetGitLog();
            Assert.AreEqual("Initial commit", Commits.FirstOrDefault().Message, "Should be 'Initial commit'");
            Assert.AreEqual("feat: added ci", Commits[4].Message, "The fith message of this Repo was: 'feat: added ci'");
            Assert.AreEqual("2020-01-24 10:19:17", Commits[4].Date.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"), "Date was '24.01.2020 11:19:17'");
            Assert.AreEqual(6, Commits[4].Files.Count, "The Commit Changed 6 files");
            Assert.AreEqual("39132a6bfcd023e3cbcf4c9515dfedd073af4aa0", Commits[4].Sha, "Sha should be equal");
            Assert.AreEqual("Jens Gödde <goedde.jens@gmail.com>", Commits[4].Headers["Author"], "Author should be 'Jens Gödde <goedde.jens@gmail.com>'");
        }
        [Test]
        public void CouldCreateChangeLog()
        {
            var changeLog = Commits.GetVersionLog().GetChangeLog();
            Assert.AreEqual("Major Release 0", Regex.Split(changeLog,"\r\n|\r|\n")[0]);
            Assert.AreEqual("\tFeatures:", Regex.Split(changeLog, "\r\n|\r|\n")[1]);
            Assert.AreEqual("\t0.6.0\t added documentation (readme.md)", Regex.Split(changeLog, "\r\n|\r|\n")[2]);
            Assert.AreEqual("\t0.5.0\t added options", Regex.Split(changeLog, "\r\n|\r|\n")[3]);
            Assert.AreEqual("\t0.6.1\t Newest Fix", Regex.Split(changeLog, "\r\n|\r|\n")[10]);
        }
    }
}