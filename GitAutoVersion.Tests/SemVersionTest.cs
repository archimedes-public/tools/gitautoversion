﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace GitAutoVersion.Tests
{
    public class SemVersionTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void CanSet()
        {
            var version = new SemVersion(1);
            Assert.AreEqual("1.0.0", version.ToString());
            version.SetMajor(2);
            Assert.AreEqual("2.0.0", version.ToString());
            version.SetMinor(2);
            Assert.AreEqual("2.2.0", version.ToString());
            version.SetPatch(2);
            Assert.AreEqual("2.2.2", version.ToString());
            version.SetMinor(3);
            Assert.AreEqual("2.3.0", version.ToString());
            version.SetMajor(3);
            Assert.AreEqual("3.0.0", version.ToString());
        }
        [Test]
        public void CanIncrement()
        {
            var version = new SemVersion(1);
            Assert.AreEqual("1.0.0", version.ToString());
            version.PatchUpdate();
            Assert.AreEqual("1.0.1", version.ToString());
            version.MinorUpdate();
            Assert.AreEqual("1.1.0", version.ToString());
            version.MajorUpdate();
            Assert.AreEqual("2.0.0", version.ToString());
        }
        [Test]
        public void CanToString()
        {
            Assert.AreEqual("1.12.123", new SemVersion(1, 12, 123).ToString());
        }
        [Test]
        public void CanParseNumbers()
        {
            for (int ma = 0; ma < 100; ma += 21)
                for (int mi = 0; mi < 100; mi += 21)
                    for (int pa = 0; pa < 100; pa += 21)
                        if (SemVersion.TryParse($"{ma}.{mi}.{pa}", out SemVersion version))
                            Assert.AreEqual($"{ma}.{mi}.{pa}", version.ToString());
        }
        [Test]
        public void CanParse()
        {
            Assert.AreEqual(false, SemVersion.TryParse(",.-", out SemVersion _), "Random Symbols");
            Assert.AreEqual(false, SemVersion.TryParse("Text", out SemVersion _), "Text");
            Assert.AreEqual(true, SemVersion.TryParse("0.0.0.0", out SemVersion _), "More numbers than tracked");
            Assert.AreEqual(true, SemVersion.TryParse("0.0.0-patch", out SemVersion _), "Patch");
            Assert.AreEqual(true, SemVersion.TryParse("0.0.0+1.2", out SemVersion _), "Build");
            Assert.AreEqual(true, SemVersion.TryParse("0.0.0-patch+1.2", out SemVersion _), "Patch & Build");
        }
        [Test]
        public void CanParsePatchAndBuild()
        {
            Assert.AreEqual(true, SemVersion.TryParse("0.0.0-patch", out SemVersion _), "Patch");
            Assert.AreEqual(true, SemVersion.TryParse("0.0.0+1.2", out SemVersion _), "Build");
            Assert.AreEqual(true, SemVersion.TryParse("0.0.0-patch+1.2", out SemVersion _), "Patch & Build");
            if (SemVersion.TryParse("0.0.0-pre", out SemVersion v))
            {
                Assert.AreEqual("pre", v.PreRelease);
            }
            if (SemVersion.TryParse("0.0.0+build", out v))
            {
                Assert.AreEqual("build", v.Build);
            }
            if (SemVersion.TryParse("0.0.0-pre+build", out v))
            {
                Assert.AreEqual("pre", v.PreRelease);
                Assert.AreEqual("build", v.Build);
            }
        }
    }
}
