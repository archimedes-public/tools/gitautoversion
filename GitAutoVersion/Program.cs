﻿using Microsoft.Extensions.CommandLineUtils;
using System;
using System.Diagnostics;
using System.Linq;

namespace GitAutoVersion
{
    class Program
    {

        static void Main(string[] args)
        {
            var app = new CommandLineApplication
            {
                Name = "GitAutoVersion",
                Description = ".NET Core Git Versioning for in CI usage."
            };
            app.HelpOption("-?|-h|--help");

            var opMas = app.Option("-mas|--major-set <trigger>", "Add a trigger for setting the major version", CommandOptionType.MultipleValue,true);
            var opMis = app.Option("-mis|--minor-set <trigger>", "Add a trigger for setting the minor version", CommandOptionType.MultipleValue, true);
            var opPas = app.Option("-pas|--patch-set <trigger>", "Add a trigger for setting the patch version", CommandOptionType.MultipleValue, true);
            var opMai = app.Option("-mai|--major-inc <trigger>", "Add a trigger for increasing the major version", CommandOptionType.MultipleValue, true);
            var opMii = app.Option("-mii|--minor-inc <trigger>", "Add a trigger for increasing the minor version", CommandOptionType.MultipleValue, true);
            var opPai = app.Option("-pai|--patch-inc <trigger>", "Add a trigger for increasing the patch version", CommandOptionType.MultipleValue, true);
            var opVes = app.Option("-ves|--version-set <trigger>", "Add a trigger for setting the complete version", CommandOptionType.MultipleValue, true);
            var opSep = app.Option("-sep|--seperator <value>", "Set the seperator between trigger and message", CommandOptionType.SingleValue, true);
            var opTar = app.Option("-t|--target-dir <path>", "Set the target directory", CommandOptionType.SingleValue, true);
            var opWor = app.Option("-w|--working-dir <path>", "Set the working directory", CommandOptionType.SingleValue, true);

            Func<(GitCommitParserConfig,GitVersionParserConfig)> GetConfigs = () => {

                var config = new GitVersionParserConfig();
                if (opMas.HasValue())
                    config.MajorSetStrings = opMas.Values.ToArray();
                if (opMis.HasValue())
                    config.MinorSetStrings = opMis.Values.ToArray();
                if (opPas.HasValue())
                    config.PatchSetStrings = opPas.Values.ToArray();
                if (opMai.HasValue())
                    config.MajorIncStrings = opMai.Values.ToArray();
                if (opMii.HasValue())
                    config.MinorIncStrings = opMii.Values.ToArray();
                if (opPai.HasValue())
                    config.PatchIncStrings = opPai.Values.ToArray();
                if (opVes.HasValue())
                    config.VersionSetStrings = opVes.Values.ToArray();
                if (opSep.HasValue())
                    config.Seperator = opSep.Value();

                var parserConfig = new GitCommitParserConfig();
                if (opTar.HasValue())
                    parserConfig.TargetDirectory = opTar.Value();
                if (opWor.HasValue())
                    parserConfig.WorkingDirectory = opWor.Value();
                return (parserConfig, config);
            };

            app.OnExecute(() =>
            {
                app.ShowHelp();
                return 0;
            });

            app.Command("parse", (command) =>
            {
                command.Description = "Parses all git commits.";
                command.HelpOption("-?|-h|--help");
                command.OnExecute(() =>
                {
                    var list = GitCommitParser.GetGitLog();
                    Console.WriteLine(GitCommitParser.GetRepoPath());
                    Console.WriteLine(list.Count);
                    foreach (var commit in list)
                    {
                        Console.WriteLine("M:" + commit.Message);
                        foreach (var header in commit.Headers)
                            Console.WriteLine(header.Key + "->" + header.Value);
                        foreach (var file in commit.Files)
                            Console.WriteLine(file.File + " ({0})", file.Status);
                        Console.WriteLine("Sha: " + commit.Sha);
                    }

                    return 0;
                });
            });
            app.Command("version", (command) =>
            {
                Version version = new Version();
                command.Description = "Gets the current version";
                command.HelpOption("-?|-h|--help");
                command.OnExecute(() =>
                {
                    var (parserConfig, config) = GetConfigs();
                    var list = GitCommitParser.GetGitLog(parserConfig);
                    Console.WriteLine(list.GetVersionLog(config).GetVersion().ToString());
                    return 0;
                });
            });
            app.Command("versionlog", (command) =>
            {
                Version version = new Version();
                command.Description = "Gets information";
                command.HelpOption("-?|-h|--help");
                command.OnExecute(() =>
                {
                    var (parserConfig, config) = GetConfigs();
                    var list = GitCommitParser.GetGitLog(parserConfig);
                    Console.Write(list.GetVersionLog().GetChangeLog());
                    return 0;
                });
            });
            app.Command("info", (command) =>
            {
                Version version = new Version();
                command.Description = "Gets information";
                command.HelpOption("-?|-h|--help");
                command.OnExecute(() =>
                {
                    Console.WriteLine("info");
                    return 0;
                });
            });

            app.Execute(args);

        }
    }
}
