﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GitAutoVersion
{

    public static class GitVersionParser
    {
        public static SemVersion GetVersion(this IEnumerable<GitVersionLog> log)
        {
            return log.LastOrDefault()?.Version ?? new SemVersion(0,0,0);
        }
        public static string GetChangeLog(this IEnumerable<GitVersionLog> versionLog)
        {
            StringBuilder builder = new StringBuilder();
            var versionLogRev = versionLog.ToList();
            versionLogRev.Reverse();
            foreach (var majorVersion in versionLogRev.GroupBy(t => t.Version.Major))
            {
                builder.AppendLine("Major Release " + majorVersion.Key + "");
                foreach (var major in majorVersion.Where(l => l.Type == GitVersionLog.LogType.Major))
                {
                    builder.AppendLine("\t" + major.Message);
                }
                builder.AppendLine("\tFeatures:");
                foreach (var minor in majorVersion.Where(l => l.Type == GitVersionLog.LogType.Minor))
                {
                    builder.AppendLine("\t" + minor.Version.ToString() + "\t" + minor.Message);
                }
                builder.AppendLine();
                builder.AppendLine("\tBugfixes:");
                foreach (var patch in majorVersion.Where(l => l.Type == GitVersionLog.LogType.Patch))
                {
                    builder.AppendLine("\t" + patch.Version.ToString() + "\t" + patch.Message);
                }
                builder.AppendLine();
            }
            return builder.ToString();
        }

        public static List<GitVersionLog> GetVersionLog(this IEnumerable<GitCommit> commits, GitVersionParserConfig? config = null )
        {
            var log = new List<GitVersionLog>();
            if(config == null)
                config = new GitVersionParserConfig();
            var version = new SemVersion(0,0,0);
            GitVersionLog.LogType logType = GitVersionLog.LogType.None;
            foreach (var commit in commits)
            {
                foreach (var line in commit.Message.Split('\n'))
                {
                    string str;
                    string message = string.Empty;
                    if (null != (str = config.PatchIncStrings.FirstOrDefault(s => line.TrimStart().StartsWith(s + config.Seperator, config.StringComparison))))
                    {
                        message = line.Substring((str + config.Seperator).Length);
                        version.PatchUpdate();
                        logType = GitVersionLog.LogType.Patch;
                    }
                    else if (null != (str = config.MinorIncStrings.FirstOrDefault(s => line.TrimStart().StartsWith(s + config.Seperator, config.StringComparison))))
                    {
                        message = line.Substring((str + config.Seperator).Length);
                        version.MinorUpdate();
                        logType = GitVersionLog.LogType.Minor;
                    }
                    else if(null != (str = config.MajorIncStrings.FirstOrDefault(s => line.TrimStart().StartsWith(s + config.Seperator, config.StringComparison))))
                    {
                        message = line.Substring((str + config.Seperator).Length);
                        version.MajorUpdate();
                        logType = GitVersionLog.LogType.Major;
                    }
                    else if(null != (str = config.PatchSetStrings.FirstOrDefault(s => line.TrimStart().StartsWith(s + config.Seperator, config.StringComparison))))
                    {
                        if (int.TryParse(line.Substring((str + config.Seperator).Length), out int patch))
                        {
                            version.SetPatch(patch);
                            logType = GitVersionLog.LogType.Patch;
                        }
                    }
                    else if(null != (str = config.MinorSetStrings.FirstOrDefault(s => line.TrimStart().StartsWith(s + config.Seperator, config.StringComparison))))
                    {
                        if (int.TryParse(line.Substring((str + config.Seperator).Length), out int minor))
                        {
                            version.SetMinor(minor);
                            logType = GitVersionLog.LogType.Minor;
                        }
                    }
                    else if(null != (str = config.MajorSetStrings.FirstOrDefault(s => line.TrimStart().StartsWith(s + config.Seperator, config.StringComparison))))
                    {
                        if (int.TryParse(line.Substring((str + config.Seperator).Length), out int major))
                        {
                            version.SetMajor(major);
                            logType = GitVersionLog.LogType.Major;
                        }
                    }
                    else if(null != (str = config.VersionSetStrings.FirstOrDefault(s => line.TrimStart().StartsWith(s + config.Seperator, config.StringComparison))))
                    {
                        if (SemVersion.TryParse(line.Substring((str + config.Seperator).Length), out SemVersion v))
                        {
                            version = v;
                            logType = GitVersionLog.LogType.Set;
                        }
                    }
                    if(str != null)
                    {
                        log.Add(new GitVersionLog() { Message = message, Version = version, Type = logType });
                    }
                }
            }
            return log;
        }
    }
}
