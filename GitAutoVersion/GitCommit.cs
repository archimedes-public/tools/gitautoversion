﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace GitAutoVersion
{
    public class GitCommit
    {
        public GitCommit()
        {
            Headers = new Dictionary<string, string>();
            Files = new List<GitFileStatus>();
            Message = string.Empty;
            Sha = string.Empty;
        }
        public Dictionary<string, string> Headers { get; set; }
        public string Sha { get; set; }
        public string Message { get; set; }
        public List<GitFileStatus> Files { get; private set; }
        // ddd MMM dd yyyy HH:mm:ss 'GMT'K '
        // Mon Jan 27 16:08:17 2020 +0100
        public DateTime Date { get => DateTime.ParseExact(Headers["Date"], "ddd MMM dd HH:mm:ss yyyy K",CultureInfo.InvariantCulture); }
    }
    public class GitFileStatus
    {
        public string Status { get; set; } = string.Empty;
        public string File { get; set; } = string.Empty;
    }
}
