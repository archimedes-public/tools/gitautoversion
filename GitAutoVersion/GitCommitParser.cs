﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace GitAutoVersion
{
    public class GitCommitParser
    {
        public static List<GitCommit> ReadOutput(string output)
        {
            GitCommit? commit = null;
            var commits = new List<GitCommit>();
            if (string.IsNullOrWhiteSpace(output))
                return commits;
            bool processingMessage = false;
            using (var strReader = new StringReader(output))
            {
                do
                {
                    var line = strReader.ReadLine();

                    if (!string.IsNullOrEmpty(line))
                    {
                        if (line.StartsWith("commit "))
                        {
                            if (commit != null)
                                commits.Add(commit);
                            commit = new GitCommit
                            {
                                Sha = line.Split(' ')[1]
                            };
                        }

                        else if (StartsWithHeader(line))
                        {
                            var header = line.Split(':')[0];
                            var val = string.Join(":", line.Split(':').Skip(1)).Trim();

                            commit!.Headers.Add(header, val);
                        }
                        else if (line.Length > 1 && char.IsLetter(line[0]) && line[1] == '\t')
                        {
                            var status = line.Split("\t")[0];
                            var file = line.Split("\t")[1];
                            commit!.Files.Add(new GitFileStatus() { Status = status, File = file });
                        } 
                        else
                        {
                            while (line != null && line.Length > 0 && line[0] == ' ')
                            {
                                commit!.Message += line.TrimStart()+"\n";
                                line = strReader.ReadLine();
                            }
                            commit!.Message = commit!.Message.Trim();
                            processingMessage = false;
                        }
                    }
                    else
                    {
                        processingMessage = true;
                    }
                }
                while (processingMessage || strReader.Peek() != -1);
                if (commit != null)
                    commits.Add(commit);
            }

            commits.Reverse();
            return commits;
        }
        private static bool StartsWithHeader(string line)
        {
            if (line.Length > 0 && char.IsLetter(line[0]))
            {
                var seq = line.SkipWhile(ch => char.IsLetter(ch) && ch != ':');
                return seq.FirstOrDefault() == ':';
            }
            return false;
        }
        public static string GetRepoPath(GitCommitParserConfig? config = null)
        {
            if (config == null)
                config = new GitCommitParserConfig();

            var psi = new ProcessStartInfo()
            {
                FileName = config.GitExecutable,
                WorkingDirectory = config.WorkingDirectory,
                Arguments = " rev-parse --show-toplevel",
                UseShellExecute = false,
                RedirectStandardOutput = true
            };
            
            Process p = Process.Start(psi);
            string output = p.StandardOutput.ReadToEnd().TrimEnd();
            p.WaitForExit();


            return output;
        }
        public static List<GitCommit> GetGitLog(GitCommitParserConfig? config=null)
        {
            if (config == null)
                config = new GitCommitParserConfig();
            var psi = new ProcessStartInfo()
            {
                FileName = config.GitExecutable,
                WorkingDirectory = config.WorkingDirectory,
                Arguments = " log --name-status " + (config.TargetDirectory ?? ""),
                UseShellExecute = false,
                RedirectStandardOutput = true,
                StandardOutputEncoding = Encoding.UTF8
            };

            Process p = Process.Start(psi);
            string output = p.StandardOutput.ReadToEnd().TrimEnd();
            if (config.TimeoutMs.HasValue)   
                p.WaitForExit(config.TimeoutMs.Value);
            else
                p.WaitForExit();

            return ReadOutput(output);
        }
        
    }
}
