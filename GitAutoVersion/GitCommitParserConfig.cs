﻿using System;

namespace GitAutoVersion
{
    public class GitCommitParserConfig
    {
        public string WorkingDirectory { get; set; }
        public string GitExecutable { get; set; }
        public string? TargetDirectory { get; set; }
        public int? TimeoutMs { get; set; }

        public GitCommitParserConfig()
        {
            WorkingDirectory = Environment.CurrentDirectory;
            GitExecutable = "git";
            TargetDirectory = null;
            TimeoutMs = 30000;
        }
    }
}
