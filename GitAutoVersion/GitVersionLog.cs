﻿namespace GitAutoVersion
{
    public class GitVersionLog
    {
        public enum LogType
        {
            None,
            Major,
            Minor,
            Patch,
            Set
        }
        public LogType Type { get; set; } = LogType.None;
        public SemVersion Version { get; set; } = new SemVersion(0,0,0);
        public string Message { get; set; } = string.Empty;
    }
}
