﻿using System;

namespace GitAutoVersion
{
    public class GitVersionParserConfig
    {
        public string[] PatchIncStrings { get; set; } = new string[] { "fix" };
        public string[] MinorIncStrings { get; set; } = new string[] { "feat" };
        public string[] MajorIncStrings { get; set; } = new string[] { "breaking change" };
        public string[] PatchSetStrings { get; set; } = new string[] { "patch" };
        public string[] MinorSetStrings { get; set; } = new string[] { "minor" };
        public string[] MajorSetStrings { get; set; } = new string[] { "major" };
        public string[] VersionSetStrings { get; set; } = new string[] { "version" };
        public string Seperator { get; set; } = ":";
        public StringComparison StringComparison { get; set; } = StringComparison.OrdinalIgnoreCase;

    }
}
