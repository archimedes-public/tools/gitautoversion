﻿using System.Text.RegularExpressions;

namespace GitAutoVersion
{
    public struct SemVersion
    {
        public int Major { get; private set; }
        public int Minor { get; private set; }
        public int Patch { get; private set; }
        public string PreRelease { get; private set; }
        public string Build { get; private set; }
        public SemVersion(int major,int minor = 0,int patch = 0, string preRelease = "",string build = "")
        {
            Major = major;
            Minor = minor;
            Patch = patch;
            PreRelease = preRelease;
            Build = build;
        }
        public void SetMajor(int major)
        {
            Major = major;
            SetMinor(0);
        }
        public void SetMinor(int minor)
        {
            Minor = minor;
            SetPatch(0);
        }
        public void SetPatch(int patch)
        {
            Patch = patch;
            PreRelease = string.Empty;
            Build = string.Empty;
        }
        public void MajorUpdate()
        {
            SetMajor(Major + 1);
        }
        public void MinorUpdate()
        {
            SetMinor(Minor + 1);
        }
        public void PatchUpdate()
        {
            SetPatch(Patch + 1);
        }
        public static bool TryParse(string s,out SemVersion version)
        {
            if (string.IsNullOrWhiteSpace(s))
            {
                version = new SemVersion();
                return false;
            }
            var regex = new Regex(@"((?>\d+\.?)+)(?>-(\w+))?(?>\+(\w+))?", RegexOptions.Singleline);
            var numberedVersion = regex.Match(s).Groups[1].Value;
            var preRelease = regex.Match(s).Groups[2].Value;
            var build = regex.Match(s).Groups[3].Value;

            var parts = numberedVersion.Split(".");
            int major, minor;
            switch (parts.Length)
            {
                case 1:
                    if (int.TryParse(parts[0], out major))
                    {
                        version = new SemVersion(major, 0, 0, preRelease, build);
                        return true;
                    }
                    break;
                case 2:
                    {
                        if (int.TryParse(parts[0], out major) &&
                            int.TryParse(parts[1], out minor))
                        {
                            version = new SemVersion(major, minor, 0, preRelease, build);
                            return true;
                        }
                        break;
                    }                 
                default:
                    if(parts.Length >= 3)
                        if (int.TryParse(parts[0], out major) &&
                            int.TryParse(parts[1], out minor) &&
                            int.TryParse(parts[2], out int patch))
                        {
                            version = new SemVersion(major, minor, patch,preRelease,build);
                            return true;
                        }
                    break;
            }
            version = new SemVersion();
            return false;
        }
        public override string ToString()
        {
            return $"{Major}.{Minor}.{Patch}{(string.IsNullOrWhiteSpace(PreRelease) ? string.Empty : ("-" + PreRelease))}{(string.IsNullOrWhiteSpace(Build) ? string.Empty : ("+" + Build))}";
        }
    }
}
