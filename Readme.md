# GitAutoVersion (GAV)

- [GitAutoVersion (GAV)](#gitautoversion-gav)
  - [What is GitAutoVersion](#what-is-gitautoversion)
  - [How to use](#how-to-use)
    - [Triggers](#triggers)
      - [Examples](#examples)
      - [Multiline](#multiline)
    - [Commandline](#commandline)
      - [Options](#options)
  - [How to download GAV via CLI](#how-to-download-gav-via-cli)
    - [Linux](#linux)
    - [Windows](#windows)

## What is GitAutoVersion
GitAutoVersion is a CLI to generate a Version from the Git commit history.
It reads specific triggers in the commit message to specify the versionchange per commit.  
It hereby follows the guidelines of [Semantic Versioning](https://semver.org/).

## How to use

### Triggers

If you want to update the Version with the Commitmessage, it should have the template: 
> `Trigger`: `Message`

Any other message will be ignored.

The following Triggers are predifined but can be configured with [options](#options):
> **Version Increasment**  
> The Version number will increase by one when encountering the trigger.
> - Major version: "breaking change"
> - Minor version: "feat", "feature"
> - Patch version: "fix"

> **Version Override**  
> The Version number will be overritten to the specified number when encountering the trigger.
> - Major version: "major"
> - Minor version: "minor"
> - Patch version: "patch"
> - Complete version: "version"

#### Examples
| Commit Message               | Previous Version | Resulting Version |
| ---------------------------- | ---------------- | ----------------- |
| breaking change: Updated API | 1.2.3            | 2.0.0             |
| feat: Added functionality    | 1.2.3            | 1.3.0             |
| fix: Fixed typo              | 1.2.3            | 1.2.4             |
| major: 3                     | 1.2.3            | 3.0.0             |
| minor: 2                     | 1.2.3            | 1.2.0             |
| patch: 1                     | 1.2.3            | 1.2.1             |
| version: 3.2.1               | 1.2.3            | 3.2.1             |
| version: 3.2                 | 1.2.3            | 3.2.0             |
| version: 3                   | 1.2.3            | 3.0.0             |

#### Multiline
Each line of a Commitmessage will be treated individually, so the following can be achived with one Message.
> Version is 1.0.0  
> feat: Some Feature resulting in 1.1.0  
> breaking change: Now its 2.0.0  
> fix: A fix message - Now its 2.0.1  

Each of the steps will be added to the [changelog]() aswell.

### Commandline 
| Command    | Description                                                     |
| ---------- | --------------------------------------------------------------- |
| version    | Get the current version from the current repository             |
| versionlog | Get the changelog for every version from the current repository |

#### Options
| Option | Description | Example Usage |
| - | - | - |
| -t/--target-dir | The version will be based only on commits that changed files within the target path.  Default is the current repository root folder. Is used to define multiple versions within one repository. | -t .\GitAutoVersion.Tests\  |
| -w/--working-dir | The working directory for the git commands. Default is the current working directory. | -w .\project\ |
| - | - | - |
| -mas/--major-set \<trigger> | Add a trigger for setting the major version | -mas "major" |
| -mis/--minor-set \<trigger> | Add a trigger for setting the minor version | -mis "minor" |
| -pas/--patch-set \<trigger> | Add a trigger for setting the patch version | -pas "patch" |
| -mai/--major-inc \<trigger> | Add a trigger for increasing the major version | -mai "breaking change" |
| -mii/--minor-inc \<trigger> | Add a trigger for increasing the minor version | -mii "feat" |
| -pai/--patch-inc \<trigger> | Add a trigger for increasing the patch version | -pai "fix" |
| -ver/--version-set \<trigger> | Add a trigger for setting the complete version | -ver "version" |
| -sep/--seperator \<value> | Set the seperator between trigger and message | -sep "=" |

All Options that add a trigger can be used multiple times:  
> ./GitAutoVersion.exe -pai "feat" -pai "feature"  

This will set `feature` and `feat` as the triggers for increasing the patch version.

## How to download GAV via CLI
### Linux

```bash
curl -L https://gitlab.com/archimedes-public/tools/gitautoversion/-/jobs/artifacts/master/download?job=linux-x64 --output gitautoversion.zip && unzip -j -o -qq gitautoversion.zip linux-x64/*

version=$(./GitAutoVersion version)
echo Version: $version
```

### Windows

> ToDo: Create Bat Script
```powershell
$f = [System.IO.File]::Create($(Get-Item -Path ".\").FullName+ "\gitautoversion.zip");
$r = [System.Net.WebRequest]::Create("https://gitlab.com/archimedes-public/tools/gitautoversion/-/jobs/artifacts/master/download?job=win-x64")
$r.AllowAutoRedirect= $true;
$r.GetResponse().GetResponseStream().CopyTo($f);
$f.Close();
Expand-Archive -LiteralPath .\gitautoversion.zip -DestinationPath .\
$version = $(.\win-x64\GitAutoVersion.exe version)
echo Version: $version

## OR (Slower)

Invoke-WebRequest -Uri "https://gitlab.com/archimedes-public/tools/gitautoversion/-/jobs/artifacts/master/download?job=win-x64" -OutFile "gitautoversion.zip"
Expand-Archive -LiteralPath .\gitautoversion.zip -DestinationPath .\
$version = $(.\win-x64\GitAutoVersion.exe version)
echo Version: $version
```